// Scroll Fx
import jump from 'jump.js'

/**
 * Estados dentro de la aplicación
 */
export const state = () => ({
	sidebar: false
})

/**
 * Datos mutables
 */
export const mutations = {
	scrollTo (state, to) {
		jump(to)
	}
}
