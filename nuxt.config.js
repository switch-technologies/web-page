module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Switch Tecnologías',
    meta: [
      { charset: 'utf-8' },
      { name: 'google-site-verification', content: 'ov-3ax6BEAjAct1lucZ42FaNDiEkjSi9jrhaqlhV4RA' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' },
      { hid: 'description', name: 'description', content: 'Switch Tecnologías' },
      { 'http-equiv': 'cache-control', content: 'public, max-age=600 s-maxage=1200' },
      { name: 'theme-color', content: '#1B45A7' },
      { name: 'og:image', content: 'https://switchtecno.com/icons/ogImage.jpg' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/favicon.png' },
      { rel: 'apple-touch-icon', size: '180x180', href: '/apple-icon-180x180.png'},
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#46CEF7' },
  /**
   * Custom CSS
   */
  css: [
    '~assets/styles/main.scss', // Columns/GRID system
  ],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/google-analytics',
    '@nuxtjs/sitemap',
    '@nuxtjs/pwa'
  ],
  'google-analytics': {
    id: 'UA-122859335-1'
  },
  sitemap: {
    path: '/sitemap.xml',
    hostname: 'https://switchtecno.com',
    cacheTime: 1000 * 60 * 15,
    gzip: true,
    generate: false, // Enable me when using nuxt generate
    routes: [
      {
        url: '/',
        changefreq: 'daily',
        priority: 1,
        lastmodISO: '2018-07-23T13:30:00.000Z'
      },
      {
        url: '/privacy',
        changefreq: 'daily',
        priority: 1,
        lastmodISO: '2018-07-23T13:30:00.000Z'
      },
      {
        url: '/about/apps',
        changefreq: 'daily',
        priority: 1,
        lastmodISO: '2018-07-23T13:30:00.000Z'
      },
      {
        url: '/about/consult',
        changefreq: 'daily',
        priority: 1,
        lastmodISO: '2018-07-23T13:30:00.000Z'
      },
      {
        url: '/about/saas',
        changefreq: 'daily',
        priority: 1,
        lastmodISO: '2018-07-23T13:30:00.000Z'
      },
      {
        url: '/about/sales',
        changefreq: 'daily',
        priority: 1,
        lastmodISO: '2018-07-23T13:30:00.000Z'
      },
      {
        url: '/about/web',
        changefreq: 'daily',
        priority: 1,
        lastmodISO: '2018-07-23T13:30:00.000Z'
      }
    ]
  },
  workbox: {
    runtimeCaching: [
      {
        urlPattern: 'https://www.google.com/.*',
        strategyOptions: {
          cacheName: 'google-cache',
          cacheExpiration: {
            maxEntries: 10,
            maxAgeSeconds: 300
          }
        }
      },
      {
        urlPattern: 'https://fonts.googleapis.com/.*',
        strategyOptions: {
          cacheName: 'google-fonts',
          cacheExpiration: {
            maxEntries: 10,
            maxAgeSeconds: 300
          }
        }
      }
    ]
  },
  /*
  ** Build configuration
  */
  build: {
    vendor: ['assets/js/float-panel.js'],
    babel: {
      presets: [
        'es2015',
        'stage-0'
      ],
      plugins: [
        ["transform-runtime", {
          "polyfil": true,
          "regenerator": true
        }]
      ]
    },
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    // CSS Bulma warnings
    postcss: {
			plugins: {
				'postcss-custom-properties': {
					warnings: false
				}
			}
		}
  }
}
