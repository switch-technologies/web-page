
<p align="center">
  <br>
    <img height="200" src="https://image.ibb.co/nEKdh8/Whats_App_Image_2018_06_20_at_6_56_09_PM.jpg" />
  <br>
  <br>
  <br>
</p>

# Switch Tecnologias - Website

Sitio informativo para resumen y publicidad de productos y servicios de la empresa.

## Notas generales

Preprocesadores utilizados
* _SASS_ (SCSS/SASS)
* _PUG_

## Construcción Setup

``` bash
# install dependencies
$ npm install # Or yarn install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

## LICENSE

MIT
> Crafted by Switch Tecnologías © Dev